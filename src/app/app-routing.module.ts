
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    loadChildren: () => import('src/app/pages/home/home.module').then(m => m.HomeModule)
  },
  {
    path: 'hero-list',
    loadChildren: () => import('src/app/pages/hero-list/hero-list.module').then(m => m.HeroListModule)
  },
  {
    path: 'detail/:heroId',
    loadChildren: () => import('src/app/pages/hero-detail/hero-detail.module').then(m => m.HeroDetailModule)
  },
  {
    path: 'create-hero',
    loadChildren: () => import('src/app/pages/create-hero/create-hero.module').then(m => m.CreateHeroModule)
  },
  {
    path: 'edit-hero/:heroId',
    loadChildren: () => import('src/app/pages/edit-hero/edit-hero.module').then(m => m.EditHeroModule)
  },
  {
  path: 'about',
  loadChildren: () => import('src/app/pages/about/about.module').then(m => m.AboutModule)
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
