import { environment } from './../../../../environments/environment.prod';
import { IPostHeroResponse } from './models/hero.models';
import { IHero } from './../../../pages/hero-list/models/hero.models';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HerosService {

  constructor(private httpClient: HttpClient) { }

  public getHeros(): Observable<IHero[]> {
    return this.httpClient.get<IHero[]>('https://628fb307dc47852365454a59.mockapi.io/character');
    // return this.httpClient.get<IHero[]>(`${environment.apiUrl}Characters`);
  }

  public getHeroById(idHero: string) {
    return this.httpClient.get<IHero>(`https://thronesapi.com/api/v2/Characters/${idHero}`);
  }

  public addHero(body: IHero): Observable<IHero> {
    return this.httpClient.post<IHero>(
      'https://628fb307dc47852365454a59.mockapi.io/character',
      body
    );
  }

  public editHero(idHero: string, body: IHero): Observable<IHero> {
    return this.httpClient.put<IHero>(
      `https://628fb307dc47852365454a59.mockapi.io/character/${idHero}`,
      body
    );
  }

  public deleteHero(idHero: string): Observable<IHero> {
    return this.httpClient.delete<IHero>(`https://628fb307dc47852365454a59.mockapi.io/character/${idHero}`);
  }
}
