import { HerosService } from './services/heros/heros.service';
import { MessageService } from './services/message.service';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';





@NgModule({
  declarations: [


  ],
  providers: [
    MessageService,
    HerosService
  ],
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule
    
  ],
  exports: [

  ]
})
export class CoreModule { }
