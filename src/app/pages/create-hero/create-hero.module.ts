import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateHeroRoutingModule } from './create-hero-routing.module';
import { CreateHeroComponent } from './create-hero.component';


@NgModule({
  declarations: [
    CreateHeroComponent
  ],
  imports: [
    CommonModule,
    CreateHeroRoutingModule,
    SharedModule
  ]
})
export class CreateHeroModule { }
