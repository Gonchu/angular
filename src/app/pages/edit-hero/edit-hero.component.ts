import { IHero } from './../../core/services/heros/models/hero.models';
import { HerosService } from './../../core/services/heros/heros.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-edit-hero',
  templateUrl: './edit-hero.component.html',
  styleUrls: ['./edit-hero.component.scss']
})
export class EditHeroComponent implements OnInit {

  public hero?: IHero;

  constructor(
    private activatedRoute: ActivatedRoute,
    private herosService: HerosService) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      const heroId = params ['heroId'];
      return this.herosService.getHeroById(heroId).subscribe((hero) => {
        this.hero = hero;
      });
    });
  }

}
