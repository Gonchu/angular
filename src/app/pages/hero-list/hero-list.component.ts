import { HerosService } from './../../core/services/heros/heros.service';
import { MessageService } from './../../core/services/message.service';
import { Component, OnInit } from '@angular/core';
import { IHero } from './models/hero.models';
import { heros } from './hero-list.config';

@Component({
  selector: 'app-hero-list',
  templateUrl: './hero-list.component.html',
  styleUrls: ['./hero-list.component.scss']
})
export class HeroListComponent implements OnInit {

  public heros?: IHero[];
  // public filteredHeros?: IHero[];
  public canModify: boolean = false;
  public filterValue: string = "";
  public message: string = '';
  public page: number = 0;
  public maxPage: number = 0;

  constructor(
    private messageService: MessageService,
    private herosService: HerosService
    ) { }

  ngOnInit(): void {
    this.getHeros();
  }

  public onModify(){
    this.canModify = !this.canModify;
  }

  public onDelete(id: string) {
  this.herosService.deleteHero(id).subscribe((hero) => {
  console.log('Eliminado', hero);
  this.getHeros();
        
  });
  // public onDelete(fullName: string) {
  //   this.heros = this.heros?.filter(hero => hero.fullName !== fullName);
  }

  // public onFilter() {
  //   this.filteredHeros = this.heros?.filter(hero => {
  //     return hero.fullName.toLowerCase().includes(this.filterValue.toLowerCase());
  //   });
  // }

  // public sendMessage() {
  //   this.messageService.setMessage(this.message);
  // }

  private  getHeros() {
    this.herosService.getHeros().subscribe((heros) => {
      this.heros = heros;
      console.log(this.heros);
      // this.filteredHeros = heros;
      // this.herosService.addHero({
      //   ...heros[0],
      //   fullName: 'prueba',
      //   family:'es una prueba'
      // }).subscribe((res) => console.log(res));
    });
  }
}
