import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IHero } from '../../models/hero.models'

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.scss']
})
export class HeroComponent implements OnInit {

  @Input() public hero?: IHero;
  @Input() public canDelete: boolean = false;
  @Output() public delete: EventEmitter<void> = new EventEmitter();

  public isSelected: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  public onDelete() {
    this.delete.emit();
    }

  public onClick() {
    this.isSelected = !this.isSelected;
  }

}
