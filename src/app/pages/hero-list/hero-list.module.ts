import { SharedModule } from './../../shared/shared.module';
import { HeroListRoutingModule } from './hero-list-routing.module';
import { HeroDetailRoutingModule } from './../hero-detail/hero-detail-routing.module';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HeroComponent } from './components/hero/hero.component';
import { HeroListComponent } from './hero-list.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [
    HeroListComponent,
    HeroComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    HeroListRoutingModule,
    SharedModule
  ]
})
export class HeroListModule { }
