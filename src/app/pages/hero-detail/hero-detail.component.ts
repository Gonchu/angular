import { Observable } from 'rxjs';
import { HerosService } from './../../core/services/heros/heros.service';
import { IHero } from './../hero-list/models/hero.models';
import { heros } from './../hero-list/hero-list.config';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.scss']
})
export class HeroDetailComponent implements OnInit {

  // public heros: IHero[] = heros;
  // public currentHero?: IHero;
  public currentHero$?: Observable<IHero>

  constructor(
    private activatedRoute:ActivatedRoute,
    private herosService: HerosService
    ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      const heroId = params['heroId'];
      this.currentHero$ = this.herosService.getHeroById(heroId);
        
      });
    };
  }


