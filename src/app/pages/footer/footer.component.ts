import { Component, OnInit } from '@angular/core';
import { ISocialNetwork } from './models/footer.models';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  public socialNetworks: ISocialNetwork[] = [
    {img: 'Facebook', url: 'https://www.facebook.com/juegodetronos/'},
    {img: 'Instagram', url: 'https://www.instagram.com/juegodetronos/?hl=es'},
    {img: 'Twitter', url: 'https://twitter.com/juegodetronos'}
  ]  

}

