export interface ISocialNetwork {
    img: string,
    url: string
}