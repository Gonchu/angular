import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilterComponent } from './components/filter/filter.component';
import { FilterPipe } from './pipes/filter.pipe';
import { PaginationPipe } from './pipes/pagination.pipe';
import { FormComponent } from './components/form/form.component';
import { NavbarComponent } from './navbar/navbar.component';



@NgModule({
  declarations: [
    FilterComponent,
    FilterPipe,
    PaginationPipe,
    FormComponent,
    NavbarComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule
  ],
  exports: [
    FilterComponent,
    FilterPipe,
    PaginationPipe,
    FormComponent
  ]
})
export class SharedModule { }
