import { Router } from '@angular/router';
import { HerosService } from './../../../core/services/heros/heros.service';
import { IHero } from './../../../pages/hero-list/models/hero.models';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  @Input() public hero?: IHero;
  @Input() public editMode: boolean = false;

  public heroForm?: FormGroup;

  constructor(
    private fb: FormBuilder,
    private herosService: HerosService,
    private router: Router
  ) { }

  public ngOnInit(): void {
    this.heroForm = this.fb.group({
      fullName: new FormControl(this.hero ? this.hero.fullName : '', [Validators.required]),
      firstName: new FormControl(this.hero ? this.hero.firstName : ''),
      lastName: new FormControl(this.hero ? this.hero.lastName : ''),
      title: new FormControl(this.hero ? this.hero.title : '', [Validators.required]),
      family: new FormControl(this.hero ? this.hero.family : '', [Validators.required]),
      imageUrl: new FormControl(this.hero ? this.hero.imageUrl : '', [Validators.required]),
    })
  }

  public saveHero(){
    const formValue = this.heroForm?.value;
    const heroAdd$ = this.editMode && this.hero
    ? this.herosService.editHero(this.hero.id, formValue)
    : this.herosService.addHero(formValue);
    heroAdd$.subscribe((hero) => {
      console.log(hero);
      this.router.navigate(['/hero-list']);
    });
  }
}
