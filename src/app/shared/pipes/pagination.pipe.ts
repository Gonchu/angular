import { PaginationService } from './../../core/services/pagination/pagination.service';
import { IHero } from './../../pages/hero-list/models/hero.models';
import { Pipe, PipeTransform } from '@angular/core';

const LIMIT_PAGE = 6;

@Pipe({
  name: 'pagination'
})
export class PaginationPipe implements PipeTransform {

  constructor(private paginationService: PaginationService) {}

  // transform(value: IHero[], page: number): IHero[] {
  //   this.paginationService.setMaxPage(Math.ceil(value.length / 10));
  //   const start = page*10;
  //   const end = (page + 1)*10;
  //   return value.slice(start, end);
  // }

  transform(value: IHero[], page: number): IHero[] {
    const init = page*LIMIT_PAGE;
    const end = (page + 1)*LIMIT_PAGE;
    return value.slice(init, end);
  }

}
