import { IHero } from './../../pages/hero-list/models/hero.models';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(value: IHero[], criteria: string): IHero[] {
    return value.filter(hero => {
      return hero.fullName.toLowerCase().includes(criteria.toLowerCase());
    });
  }

}
